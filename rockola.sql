-- MySQL Script generated by MySQL Workbench
-- Thu Sep  1 20:26:43 2022
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema rockola
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema rockola
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `rockola` DEFAULT CHARACTER SET utf8 ;
USE `rockola` ;

-- -----------------------------------------------------
-- Table `rockola`.`usuarios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rockola`.`usuarios` (
  `idusuario` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `apellido` VARCHAR(45) NULL,
  `usuario` VARCHAR(45) NOT NULL,
  `fechaNacimiento` VARCHAR(45) NULL,
  `correo` VARCHAR(100) NULL,
  `password` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`idusuario`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `rockola`.`tipo_usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rockola`.`tipo_usuario` (
  `idtipo` INT NOT NULL AUTO_INCREMENT,
  `admin` VARCHAR(45) NULL,
  `frecuencia` VARCHAR(45) NULL,
  `invitado` VARCHAR(45) NULL,
  `usuarios_idusuario` INT NOT NULL,
  PRIMARY KEY (`idtipo`),
  INDEX `fk_tipo_usuario_usuarios1_idx` (`usuarios_idusuario` ASC) VISIBLE,
  CONSTRAINT `fk_tipo_usuario_usuarios1`
    FOREIGN KEY (`usuarios_idusuario`)
    REFERENCES `rockola`.`usuarios` (`idusuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `rockola`.`generos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rockola`.`generos` (
  `idgeneros` INT NOT NULL AUTO_INCREMENT,
  `Nombregenero` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idgeneros`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `rockola`.`canciones`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rockola`.`canciones` (
  `idcanciones` INT NOT NULL AUTO_INCREMENT,
  `nombreCancion` VARCHAR(45) NOT NULL,
  `generos_idgeneros` INT NOT NULL,
  PRIMARY KEY (`idcanciones`, `generos_idgeneros`),
  INDEX `fk_canciones_generos1_idx` (`generos_idgeneros` ASC) VISIBLE,
  CONSTRAINT `fk_canciones_generos1`
    FOREIGN KEY (`generos_idgeneros`)
    REFERENCES `rockola`.`generos` (`idgeneros`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `rockola`.`info_canciones`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rockola`.`info_canciones` (
  `idinfo_canciones` INT NOT NULL AUTO_INCREMENT,
  `duracion` INT NULL,
  `fecha` DATETIME NULL,
  `autor` VARCHAR(45) NOT NULL,
  `productor` VARCHAR(45) NULL,
  `masescuchadas` TINYINT NULL,
  `info_cancionescol` VARCHAR(45) NULL,
  `canciones_idcanciones` INT NOT NULL,
  `canciones_generos_idgeneros` INT NOT NULL,
  PRIMARY KEY (`idinfo_canciones`),
  INDEX `fk_info_canciones_canciones1_idx` (`canciones_idcanciones` ASC, `canciones_generos_idgeneros` ASC) VISIBLE,
  CONSTRAINT `fk_info_canciones_canciones1`
    FOREIGN KEY (`canciones_idcanciones` , `canciones_generos_idgeneros`)
    REFERENCES `rockola`.`canciones` (`idcanciones` , `generos_idgeneros`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
